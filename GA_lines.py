import time
from PIL import Image, ImageDraw, ImageOps
from scipy.spatial import distance
import numpy as np
import numpy.random as rd
import os

X_OFFSET = 10  # horizontal length of the line
Y_OFFSET = 16  # vertical length of the line
LINE_WIDTH = 1  # width of the line
COLOR_VAR = 10  # how the average color of the line varies
TRANSPARENCY_MIN = 100  # transparency of the line is random. This variable is random's minimum
TRANSPARENCY_MAX = 230  # and this one is the maximum
BACKGROUND_CLR = 'black'  # set 'black', 'white', or set to 'avg' to get color-average of the input picture
EPOCHS = 100000  # number of epochs to calculate (num of epochs = num of mutations succeeded)
IMG = 'target/planet.jpg'


# Defines an initial population as a blank canvas of a given color.
#
# input String
# returns PIL
def initial_population(clr):
    if clr == 'avg':
        clr = get_average_color(0, 0, min(original_img.shape[0], original_img.shape[1]), original_img)
        clr = (int(clr[0]), int(clr[1]), int(clr[2]))
    background = Image.new('RGB', (original_img.shape[0], original_img.shape[1]), clr)

    return background


# Gets the color-average of a square defined by (x, y) of bottom-left corner and (n) as vertical and horizontal offset.
#
# input int, int, int, PIL
# returns [float, float, float]
def get_average_color(x, y, n, image):
    r, g, b = 0, 0, 0
    count = 0

    for s in range(x, x + n - 1):
        if s >= image.shape[0]:
            count += 1
            break
        for t in range(y, y + n - 1):
            if t >= image.shape[1]:
                count += 1
                break
            pixlr, pixlg, pixlb = image[s, t]
            r += pixlr
            g += pixlg
            b += pixlb
            count += 1
    if count <= 0:
        count = 1
    return [(r / count), (g / count), (b / count)]


# Creates a random mutation for a given image by drawing a line on it.
#
# input PIL
# returns PIL
def mutate(img):
    mutation = img.copy()
    img_draw = ImageDraw.Draw(mutation, 'RGBA')

    x1 = rd.randint(low=0, high=original_img.shape[0])
    y1 = rd.randint(low=0, high=original_img.shape[1])
    x_offset = X_OFFSET
    y_offset = Y_OFFSET
    x2 = min(x1 + x_offset, original_img.shape[0] - 1)
    y2 = min(y1 + y_offset, original_img.shape[1] - 1)

    if rd.randint(low=0, high=2) == 0:
        fill_color = (rd.randint(low=original_img[x1][y1][0] - COLOR_VAR, high=original_img[x1][y1][0] + COLOR_VAR),
                      rd.randint(low=original_img[x1][y1][1] - COLOR_VAR, high=original_img[x1][y1][1] + COLOR_VAR),
                      rd.randint(low=original_img[x1][y1][2] - COLOR_VAR, high=original_img[x1][y1][2] + COLOR_VAR),
                      rd.randint(low=TRANSPARENCY_MIN, high=TRANSPARENCY_MAX))
        img_draw.line([x1, y1, x2, y2], fill=fill_color, width=LINE_WIDTH)
    else:
        fill_color = (
        rd.randint(low=original_img[x1][int(y2)][0] - COLOR_VAR, high=original_img[x1][int(y2)][0] + COLOR_VAR),
        rd.randint(low=original_img[x1][int(y2)][1] - COLOR_VAR, high=original_img[x1][int(y2)][1] + COLOR_VAR),
        rd.randint(low=original_img[x1][int(y2)][2] - COLOR_VAR, high=original_img[x1][int(y2)][2] + COLOR_VAR),
        rd.randint(low=TRANSPARENCY_MIN, high=TRANSPARENCY_MAX))
        img_draw.line([x1, y2, x2, y1], fill=fill_color, width=LINE_WIDTH)

    return mutation


# Fitness function based on Euclidean Distance. Compares with the original image.
#
# input PIL
# returns float
def fitness(mutation):
    mutation = np.array(ImageOps.mirror(mutation.rotate(270)))
    mutation = mutation.reshape(mutation.shape[0] * mutation.shape[1] * mutation.shape[2])
    return distance.euclidean(mutation, fitness_vector)


if __name__ == '__main__':

    # open image
    original_img = Image.open(IMG)
    original_img = np.array(original_img)
    print("Image shape is " + str(original_img.shape))
    fitness_vector = original_img.reshape(original_img.shape[0] * original_img.shape[1] * original_img.shape[2])
    print("Fitness vector is " + str(fitness_vector))
    print("Fitness vector shape is " + str(fitness_vector.shape))

    # start evolution
    population = initial_population(BACKGROUND_CLR)
    start_time = time.time()
    epoch = 0
    fails = 0

    for i in range(300000000):
        if epoch == EPOCHS:
            break

        mutation = mutate(population)
        fitness_mutation = fitness(mutation)

        if fitness_mutation < fitness(population):
            population = mutation
            epoch += 1
            if epoch % 10 == 0:
                print("epoch " + str(epoch))
                print("time spent: " + str(time.strftime("%H:%M:%S", time.gmtime(time.time() - start_time))))
                print("last_mutation_fitness: " + str(fitness_mutation))
                print("failed mutations (this epoch): " + str(fails))
                fails = 0
                if epoch % 500 == 0:
                    if not os.path.exists("generated"):
                        os.makedirs("generated")
                    ImageOps.mirror(population.rotate(270)).save("generated/gen_" + IMG, 'JPEG')
                    print("\n\nSAVED\n\n")
        else:
            fails += 1

    population = ImageOps.mirror(population.rotate(270))
    population.show()

    # save the generated image to /generated/ folder
    if not os.path.exists("generated"):
        os.makedirs("generated")
    population.save("generated/gen_" + IMG, 'JPEG')

