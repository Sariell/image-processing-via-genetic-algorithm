# Image Processing via Genetic Algorithm

The program uses Genetic Algorithm to process an input image using two types of lines.

<img src="target/monkey.jpg" width="400" /> <img src="generated/gen_target/monkey.jpg" width="400" />  
<img src="target/house.jpg" width="200" /> <img src="generated/gen_target/house.jpg" width="200" />  <img src="target/plane.jpg" width="200" /> <img src="generated/gen_target/plane.jpg" width="200" />  


# How to run
Install requirements:

`pip install -r requirements.txt`

Run program:

`python GA_lines.py`

Or just go to https://colab.research.google.com/drive/1t5Zaelxsu_EVCrF0eDj9iBSqVG_0pX8R and run the code on one of your pictures.

# Implementation details

The image is processed mainly using **Pillow** and **numpy** libraries, by randomly painting two shapes of a line on the canvas. 

## Program parameters:

`X_OFFSET` – horizontal length of the line  
`Y_OFFSET` – vertical length of the line  
`LINE_WIDTH` – width of the line  
`COLOR_VAR` – variance of line color relative to the average in the close area. It will be random of range `(avg - COLOR_VAR, avg + COLOR_VAR)`  
`TRANSPARENCY_MIN` – transparency of the line is uniform random. This variable is random's minimum  
`TRANSPARENCY_MAX` – and this one is the maximum  
`BACKGROUND_CLR` – set 'black', 'white', or set to 'avg' to get color-average of the input picture  
`EPOCHS` – number of epochs to calculate (num of epochs = num of mutations succeeded)  
`IMG` – Path to the target image

## Algorithm:
1. Create a blank canvas of color set in `COLOR_VAR` (it will be initial population)
2. Create a copy and mutate it by adding a line in a random place
3. Check if mutation is successful by calculating Euclidean Distance between the mutation and the input image
4. If mutation is successful, set canvas as a mutation
5. Save (overwrite) image every 500 epochs
6. Loop thorugh points 2-6 until epoch counter reaches `EPOCHS` constant



